import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class Table1Service {

  constructor(private http: HttpClient) {
  }

  getConfig() {
  return this.http.get('http://patovega.com/prueba_frontend/array.php');
}
}
