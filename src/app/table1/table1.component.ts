import { Component, OnInit } from '@angular/core';
import { Table1Service } from './table1.service';

@Component({
  selector: 'app-table1',
  templateUrl: './table1.component.html',
  styleUrls: ['./table1.component.css']
})

export class Table1Component implements OnInit {
  dataTable: any = [];
  arrayTable: any = [];
  numbers: any = [];
  repeatNumber = {};

  constructor(private table1: Table1Service) {
  }

  ngOnInit() {
    this.table1.getConfig()
      .subscribe((data: any) => {
        this.dataTable = data;
        this.count_elements(data.data);
        this.row_builder();
        this.sort_array(data.data);
      });
  }

  row_builder() {
    let n = 0;
    this.dataTable.data.forEach((el) => {
      let exist = this.arrayTable.some((r) => r.number === el);
      if (!exist) {
        let row = {
          n: n,
          number: el,
          quantity: this.repeatNumber[el],
          firstPosition: this.dataTable.data.indexOf(el),
          lastPosition: this.dataTable.data.lastIndexOf(el),
          elementClass: this.select_class(this.repeatNumber[el])
        };
        this.arrayTable.push(row);
          console.log("EL", el);
        n += 1;
      }
      
    });
  }

  select_class(repetitions) {
    if(repetitions >= 2) {
      return 'bg-success';
    }

    if(repetitions === 1) {
      return 'bg-warning';
    }

    if(repetitions < 1 ) {
      return 'bg-secondary';
    }

  }

  count_elements(arr) {
    arr.forEach((num) => { 
      if (this.repeatNumber[num] === undefined) {
        this.repeatNumber[num] = 1;
      } else {
        this.repeatNumber[num] = this.repeatNumber[num] + 1;
      }
    });
    // console.log('CONTADOR', this.repeatNumber);
  }

  // Método de ordenamiento burbuja
  sort_array(arr) {
    let len = arr.length;

    for (let i = 0; i < len; i++) {
      for (let j = 0; j < len - i - 1; j++) {
        if (arr[j] > arr[j + 1]) {
          let temp = arr[j];
          arr[j] = arr[j + 1];
          arr[j + 1] = temp;
        }
      }
    }
    // console.log('SORT', arr);
  }
}
