import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { Table1Component } from './table1/table1.component';
import { Table2Component } from './table2/table2.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'table1',
    component: Table1Component
  },
  {
    path: 'table2',
    component: Table2Component
  },
  {
      path: '**',
    component: HomeComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    [RouterModule.forRoot(routes)],
    CommonModule,
  ],

  exports: [RouterModule]
})
export class AppRoutingModule { }
