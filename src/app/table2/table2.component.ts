import { Component, OnInit } from '@angular/core';
import { Table2Service } from './table2.service';

@Component({
  selector: 'app-table2',
  templateUrl: './table2.component.html',
  styleUrls: ['./table2.component.css']
})
export class Table2Component implements OnInit {
  dataTable: any = [];
  abecedario: any = [];
  countLetter: any = [];
  constructor(private table2: Table2Service) {
  }

  ngOnInit() {
    this.build_headers();
    this.table2.getConfig()
      .subscribe((data: any) => {
        this.dataTable = JSON.parse(data.data); 
        this.abc();
      });
      console.log(this.dataTable)      

  }


  abc() {
    this.dataTable.forEach((element, index) => {
      let abc = [];
      for (let i: any = 65; i <= 90; i++) {
        let letter = String.fromCharCode(i);
        let quantity = this.count_letter(letter, element.paragraph.toUpperCase());
        let letter_obj = {
          letter: letter,
          quantity: quantity
        };
        abc.push(letter_obj);
      }
      this.rowBuilder(index, abc);
    });
  }

  count_letter(letter, phrase) {
    let expresion = `[${letter}]`;
    let regex = new RegExp(expresion, 'g');
    let res = phrase.match(regex);
    if (res === null) {
      return 0;
    } else {
      return res.length;
    }
  }

  build_headers() {
    for (let i: any = 65; i <= 90; i++) {
      this.abecedario.push(String.fromCharCode(i));
    }
  }

  rowBuilder(index, abecedario) {
    let row = {
      index: (index + 1),
      abc: abecedario
    };
    this.countLetter.push(row);
  }
}
